<script>
window.onload = function() {
			if(localStorage.authToken == null){
      	alert('You are not logged in.');
        location.href="[WHERE TO REDIRECT THE USER IF NOT LOGGED IN]"}
        else
      SendToXano();
}
  
function SendToXano() { 
    fetch("[YOUR XANO ENDPOINT URL TO CALL FOR LOGGED IN USER]", {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.authToken
        }
        })

        .then(res => res.json())
				.then(json => {
        	const xanoResponse = json;
        	console.log(xanoResponse);
        	document.getElementById('user').textContent = JSON.stringify(xanoResponse);})
}
</script>
