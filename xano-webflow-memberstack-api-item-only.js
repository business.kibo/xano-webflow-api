// Get Item from List view using Search Parameter
var myUrl = new URL(document.location.href);
var myParam = myUrl.searchParams.get("id") || 1;


// Load API Endpoints from Xano to get the individual item and associated comments
let xanoUrl = new URL('https://x715-fe9c-6426.n7.xano.io/api:FFH5FMYr/item/' + myParam); // REPLACE WITH YOUR API ENDPOINT

function getItem() {
    // Create a request variable and assign a new XMLHttpRequest object to it.
    var request = new XMLHttpRequest()


    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', xanoUrl.toString(), true)

    request.onload = function() {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {

            var elms = document.querySelectorAll("[id='item-id']");
            for(var i = 0; i < elms.length; i++) 
            elms[i].value = data.id // <-- whatever you need to do here.

            // document.getElementById("item-id").value = data.id
            // Map a variable called itemContainer to the Webflow element called "Item-Container"
            const itemContainer = document.getElementById("Item-Container")

            // For each restaurant, create a div called card and style with the "Sample Card" class
            const item = document.getElementById('samplestyle')

            const img = item.getElementsByTagName('IMG')[0]
            img.src = data.banner.url + "?tpl=big:box";
            

            // Create an h1 and set the text content to the film's title
            const h3 = item.getElementsByTagName('H3')[0]
            h3.textContent = data.name;

            // Create a p and set the text content to the film's description
            const p = item.getElementsByTagName('P')[0]
            p.textContent = `${data.description.substring(0, 240)}` // Limit to 240 chars

            // Append the card to the div with "Item-Container" id
            itemContainer.appendChild(item);


        } else {
            console.log('error')
        }
    }

    // Send request
    request.send()

}


// This fires all of the defined functions when the document is "ready" or loaded
(function() {
    setTimeout(() => {
getItem();
}, 250)
})();


